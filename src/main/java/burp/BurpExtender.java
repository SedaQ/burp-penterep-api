package burp;

import static spark.Spark.*;
import spark.Filter;

import java.awt.Button;
import java.awt.Component;
import java.awt.Frame;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;


public class BurpExtender
		implements IBurpExtender, IHttpListener, IProxyListener, IScannerListener, 
		IExtensionStateListener {


	private IExtensionHelpers helpers;

	private IBurpExtenderCallbacks callbacks;
	private PrintWriter stdout;
	private PrintWriter stderr;

	ObjectMapper objectMapper = new ObjectMapper();

	private HttpRequestResponse rqst;
	private String domena;
	private String target;
	String pattern = "(http|https)(:\\/\\/)([^\\/]+):";
	Pattern r = Pattern.compile(pattern);
	Matcher m;

	//
	// implement IBurpExtender
	//
	@Override
	public void registerExtenderCallbacks(IBurpExtenderCallbacks callbacks) {

		// escaping Json
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

		DefaultPrettyPrinter printer = new DefaultPrettyPrinter();
		DefaultPrettyPrinter.Indenter i = new DefaultIndenter("  ", "\n");
		printer.indentArraysWith(i);
		printer.indentObjectsWith(i);

		objectMapper.setDefaultPrettyPrinter(printer);

		// extension helpers objekt
		helpers = callbacks.getHelpers();

		// callbacks objekt
		this.callbacks = callbacks;

		// set our extension name
		callbacks.setExtensionName("Custom REST API");

		stdout = new PrintWriter(callbacks.getStdout(), true);
		stderr = new PrintWriter(callbacks.getStderr(), true);

		//callbacks.addSuiteTab(new MyTab());
		//callbacks.customizeUiComponent();
		
		// register HTTP listener
		callbacks.registerHttpListener(this);

		// register Proxy listener
		callbacks.registerProxyListener(this);

		// register Scanner listener
		callbacks.registerScannerListener(this);

		// register listeneru zmien modulu ako nacitanie,odpojenie
		callbacks.registerExtensionStateListener(this);
		
		// websocket
		webSocket("/websocket", new MyWebSocket(callbacks));
		init();
	
		// spark

		after((Filter) (request, response) -> {
			response.header("Access-Control-Allow-Origin", "*");
			response.header("Access-Control-Allow-Methods", "GET");
		});

		post("/intruder", (request, response) -> {
			rqst = deserializeObject(request.body());
			callbacks.sendToIntruder(rqst.getHttpService().getHost(), rqst.getHttpService().getPort(),
					rqst.getHttpService().usesHttps(), rqst.getRequest(), null);
			return "Request pridan� do Intruderu.";
		});

		post("/repeater", (request, response) -> {
			rqst = deserializeObject(request.body());
			callbacks.sendToRepeater(rqst.getHttpService().getHost(), rqst.getHttpService().getPort(),
					rqst.getHttpService().usesHttps(), rqst.getRequest(), null);
			return "Request pridan� do Repeateru.";
		});
		post("/comparer", (request, response) -> {
			rqst = deserializeObject(request.body());
			callbacks.sendToComparer(rqst.getRequest());
			return "Request pridan� do Compareru.";
		});

		post("/spider/:domain", (request, response) -> {
			domena = ("https://" + request.params(":domain"));
			callbacks.sendToSpider(makeUrlFromString(domena));
			return "Request aktivoval crawlovanie str�nky.";
		});

		post("/scanner/active", (request, response) -> {
			rqst = deserializeObject(request.body());
			callbacks.doActiveScan(rqst.getHttpService().getHost(), rqst.getHttpService().getPort(),
					rqst.getHttpService().usesHttps(), rqst.getRequest());
			return "Request aktivoval akt�vny sken.";    
		});

		post("/scanner/passive", (request, response) -> {
			
			rqst = deserializeObject(request.body());
			try {
				callbacks.doPassiveScan(rqst.getHttpService().getHost(), rqst.getHttpService().getPort(), rqst.getHttpService().usesHttps(), rqst.getRequest(), rqst.getResponse());
			} catch (Exception e) {
				stdout.print(e);
				stdout.print("Ak �iados� neobsahuje v poli response �iadnu odpove�, pou�ite hodnotu \"\" vo va�om json objekte\n"
						+ "If there is no response to be analyzed, set it to '\"\"' in your json data");
			}
			
			return "Request aktivoval pas�vny sken.";
		
		});

		get("/scanner/issues/:domain", (request, response) -> {
			List<IScanIssue> DomainIssues = new ArrayList<>();
			for (IScanIssue item : callbacks.getScanIssues(null)) {
				if (item.getUrl().toString().toLowerCase().contains(request.params(":domain").toLowerCase())) {
					DomainIssues.add(item);
				}
			}
			response.type("application/json");
			return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(DomainIssues);
		});

		get("/scanner/issues", (request, response) -> {
			response.type("application/json");
			return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(callbacks.getScanIssues(null));
		});

		
		
		post("/scope/:domain", (request, response) -> {
			domena = ("https://" + request.params(":domain"));
			callbacks.includeInScope(makeUrlFromString(domena));
			return "target " + request.params(":domain") + " bola pridan� do Scope";
		});
		
		
		
		delete("/scope/:domain", (request, response) -> {
			domena = ("https://" + request.params(":domain"));
			callbacks.excludeFromScope(makeUrlFromString(domena));
			return "target " + request.params(":domain") + " bola vyl��en� zo Scope";
		});

		get("/scanner/total", (request, response) -> {
			response.type("application/json");
			return "{\"name\":\"totalIssues\", \"value\":" + Integer.toString(callbacks.getScanIssues(null).length)
					+ "}";
		});

		get("/target", (request, response) -> {
			response.type("application/json");
			return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(callbacks.getSiteMap(null));
		});

		get("/target/:domain", (request, response) -> {
			response.type("application/json");
			target = request.params(":domain");
			List<IHttpRequestResponse> DomainItems = new ArrayList<IHttpRequestResponse>();
			
			for (IHttpRequestResponse item : callbacks.getSiteMap(null)) {
				m = r.matcher(helpers.analyzeRequest(item).getUrl().toString());
				if(m.find()) {
					if(m.group(3).contains(target)){
						DomainItems.add(item);
					}
				}
			}
	
				return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(DomainItems);
		});

		
		get("/targetlist", (request, response) -> {
			response.type("application/json");
			HashSet<String> SiteMapDomains = new HashSet<>();
			for (IHttpRequestResponse item : callbacks.getSiteMap(null)) {
				SiteMapDomains.add(item.getHttpService().getHost());
			}
			return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(SiteMapDomains);

		});

		get("/target/issuesinfo/:domain", (request, response) -> {
			response.type("application/json");

			Set<jsonDomainInfoObj> DomainIssuesInfo = new HashSet<>();
			for (IScanIssue item : callbacks.getScanIssues(null)) {
				if (item.getUrl().toString().toLowerCase().contains(request.params(":domain").toLowerCase())) {
					jsonDomainInfoObj domainNode = new jsonDomainInfoObj();
					domainNode.setName(item.getIssueName());
					domainNode.setSeverity(item.getSeverity());
					DomainIssuesInfo.add(domainNode);
				}

			}

			return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(DomainIssuesInfo);
		});

		get("/graph/:domain", (request, response) -> {
			SeverityInfo sInfo = new SeverityInfo();
			List<IScanIssue> DomainIssues = new ArrayList<>();
			for (IScanIssue item : callbacks.getScanIssues(null)) {
				if (item.getUrl().toString().toLowerCase().contains(request.params(":domain").toLowerCase())) {
					DomainIssues.add(item);
				}
			}

			for (IScanIssue iScanIssue : DomainIssues) {

				stdout.println(iScanIssue.getSeverity());
				switch (iScanIssue.getSeverity().toLowerCase()) {
				case "low":
					sInfo.addLow();
					break;
				case "medium":
					sInfo.addMedium();
					break;
				case "high":
					sInfo.addHigh();
					break;
				case "information":
					sInfo.addInformation();
					break;
				case "false positive":
					sInfo.addFalsePositive();
					break;
				default:
					break;
				}
			}

			List<jsonHelperObj> Data = new ArrayList<>();
			jsonHelperObj Low = new jsonHelperObj();
			Low.setName("Low");
			Low.setValue(sInfo.getLow());
			Low.setColor("hsl(48, 100%, 55%)");

			jsonHelperObj Medium = new jsonHelperObj();
			Medium.setName("Medium");
			Medium.setValue(sInfo.getMedium());
			Medium.setColor("hsl(30, 100%, 60%)");

			jsonHelperObj High = new jsonHelperObj();
			High.setName("High");
			High.setValue(sInfo.getHigh());
			High.setColor("hsl(0, 100%, 60%)");

			jsonHelperObj Information = new jsonHelperObj();
			Information.setName("Information");
			Information.setValue(sInfo.getInformation());
			Information.setColor("hsl(200, 100%, 75%)");

			jsonHelperObj FalsePositive = new jsonHelperObj();
			FalsePositive.setName("FalsePositive");
			FalsePositive.setValue(sInfo.getFalsePositive());
			FalsePositive.setColor("hsl(0, 0%, 75%)");

			Data.add(Low);
			Data.add(Medium);
			Data.add(High);
			Data.add(Information);
			Data.add(FalsePositive);

			response.type("application/json");
			return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(Data);
		});

	}
	// IHttpListener

	@Override
	public void processHttpMessage(int toolFlag, boolean messageIsRequest, IHttpRequestResponse messageInfo) {
		stdout.println((messageIsRequest ? "HTTP request to " : "HTTP response from ") + messageInfo.getHttpService()
				+ " [" + callbacks.getToolName(toolFlag) + "]");

	}

	// IProxyListener

	@Override
	public void processProxyMessage(boolean messageIsRequest, IInterceptedProxyMessage message) {

	}

	// IScannerListener

	@Override
	public void newScanIssue(IScanIssue issue) {
		stdout.println("New scan issue: " + issue.getIssueName());
	}

	// IExtensionStateListener

	@Override
	public void extensionUnloaded() {
		stop();
		//delay to make sure the server unloads correctly
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		stdout.println("Spark server stopped");
		stdout.println("Extension was unloaded");
	}

	public HttpRequestResponse deserializeObject(String json) {
		HttpRequestResponse requestResponse = new HttpRequestResponse();
		try {
			requestResponse = objectMapper.readValue(json, HttpRequestResponse.class);

		} catch (JsonProcessingException e) {
			stderr.println(e);
			e.printStackTrace();
		}

		return requestResponse;
	}

	public URL makeUrlFromString(String str) {
		URL url = null;
		try {
			url = new URL(str);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return url;
	}


}
