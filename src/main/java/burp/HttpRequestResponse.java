package burp;


public class HttpRequestResponse implements MyHttpRequestResponse {

	private byte[] request;
	private byte[] response;
	private String commment;
	private String highlight;
	private HttpService httpService;

	public HttpRequestResponse() {
	}
	
	public byte[] getRequest() {
		return this.request;
	}

	public void setRequest(byte[] request) {
		this.request = request;

	}

	public byte[] getResponse() {
		return this.response;
	}

	public void setResponse(byte[] response) {
		this.response = response;
	}

	public String getComment() {
		return this.commment;
	}

	public void setComment(String comment) {
		this.commment = comment;
	}

	public String getHighlight() {
		return this.highlight;
	}

	public void setHighlight(String color) {
		this.highlight = color;
	}

	
	public MyHttpService getHttpService() {
		return this.httpService;
	}

	
	public void setHttpService(HttpService httpService) {
		this.httpService = httpService;
	}
	
	public void setHttpService(String host, int port, String protocol) {
		this.httpService = new HttpService();
	}
	
}
