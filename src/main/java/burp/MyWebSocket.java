package burp;

import org.eclipse.jetty.websocket.api.*;
import org.eclipse.jetty.websocket.api.annotations.*;
import org.thymeleaf.standard.processor.AbstractStandardDoubleAttributeModifierTagProcessor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;




@WebSocket
public class MyWebSocket{

	IBurpExtenderCallbacks callbacks;
	ObjectMapper objectMapper = new ObjectMapper();
	String domena="";
	
	
	public MyWebSocket(IBurpExtenderCallbacks callbacks) {
		this.callbacks = callbacks;
		
	}
	
	
	public void getData(Session session) {

		HashSet<String> SiteMapDomains = new HashSet<>();

		for (IHttpRequestResponse item : this.callbacks.getSiteMap(null)) {
			SiteMapDomains.add(item.getHttpService().getHost());
		}
		
		
		SeverityInfo sInfo = new SeverityInfo();
		List<IScanIssue> DomainIssues = new ArrayList<>();
		for (IScanIssue item : callbacks.getScanIssues(null)) {
			if (item.getUrl().toString().toLowerCase().contains(domena.toLowerCase())) {
				DomainIssues.add(item);
			}
		}

		for (IScanIssue iScanIssue : DomainIssues) {

			switch (iScanIssue.getSeverity().toLowerCase()) {
			case "low":
				sInfo.addLow();
				break;
			case "medium":
				sInfo.addMedium();
				break;
			case "high":
				sInfo.addHigh();
				break;
			case "information":
				sInfo.addInformation();
				break;
			case "false positive":
				sInfo.addFalsePositive();
				break;
			default:
				break;
			}
		}

		List<jsonHelperObj> Data = new ArrayList<>();
		jsonHelperObj Low = new jsonHelperObj();
		Low.setName("Low");
		Low.setValue(sInfo.getLow());
		Low.setColor("hsl(48, 100%, 55%)");

		jsonHelperObj Medium = new jsonHelperObj();
		Medium.setName("Medium");
		Medium.setValue(sInfo.getMedium());
		Medium.setColor("hsl(30, 100%, 60%)");

		jsonHelperObj High = new jsonHelperObj();
		High.setName("High");
		High.setValue(sInfo.getHigh());
		High.setColor("hsl(0, 100%, 60%)");

		jsonHelperObj Information = new jsonHelperObj();
		Information.setName("Information");
		Information.setValue(sInfo.getInformation());
		Information.setColor("hsl(200, 100%, 75%)");

		jsonHelperObj FalsePositive = new jsonHelperObj();
		FalsePositive.setName("FalsePositive");
		FalsePositive.setValue(sInfo.getFalsePositive());
		FalsePositive.setColor("hsl(0, 0%, 75%)");

		Data.add(Low);
		Data.add(Medium);
		Data.add(High);
		Data.add(Information);
		Data.add(FalsePositive);


		Set<jsonDomainInfoObj> DomainIssuesInfo = new HashSet<>();
		
		for (IScanIssue item : callbacks.getScanIssues(null)) {
			if (item.getUrl().toString().toLowerCase().contains(domena.toLowerCase())) {
				// DomainIssuesInfo.add(item.getIssueName());

				jsonDomainInfoObj domainNode = new jsonDomainInfoObj();
				domainNode.setName(item.getIssueName());
				domainNode.setSeverity(item.getSeverity());
				DomainIssuesInfo.add(domainNode);
			}

		}

		
		
		ArrayList<String> list = new ArrayList<String>();
		
		
		//0
		list.add("{\"name\":\"totalIssues\", \"value\":" + Integer.toString(callbacks.getScanIssues(null).length)
		+ "}");
		
		//1
		try {
			list.add(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(SiteMapDomains));
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//2
		try {
			list.add(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(Data));
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		//3
		try {
			list.add(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(DomainIssuesInfo));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		System.out.println(domena);
		try {
			System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(DomainIssuesInfo));
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println(list);
		
		*/
		
		try {
			session.getRemote().sendString(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(list));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	
		
		
	}
	
    // Store sessions if you want to, for example, broadcast a message to all users
    private static final Queue<Session> sessions = new ConcurrentLinkedQueue<>();

    @OnWebSocketConnect
    public void connected(Session session) {
        sessions.add(session);
        System.out.println("New connection");
        
        long delay = 1;
        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(1);
        scheduledThreadPool.scheduleAtFixedRate(() -> getData(session), 0, delay, TimeUnit.SECONDS);		    
    }

    @OnWebSocketClose
    public void closed(Session session, int statusCode, String reason) {
        sessions.remove(session);
    }

    @OnWebSocketMessage
    public void message(Session session, String message) throws IOException {
        System.out.println("Got: " + message);   // Print message
        domena = message;
        
    
     
        
    }

}
