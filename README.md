# README

This module was developed to aid the transfer of data _to_ and _from_ Burp Suite using REST style  requests. Burp Suite contains it's own so called modules, such as Target, Repeater, Intruder, Comparer or Scanner. This module allows transfer of HTTP requests in JSON form into the Burp Suite's modules, allowing user to get advantage of using Burp's tools on requests or responses that were not passed through Burp Suite itself. This also means that it's possible to automate this process.   

The module utilizes a server listening on port 4567, accessible through any HTTP client tool on the localhost address. Requests invoking actions on requests or responses embedded in the message body, such as sending to Repeater or Scanner, must contain JSON object which respresents the request user wishes to input into Burp. Examples are in the Example section.

## Features

Following table describes actions the module is capable of performing along with the url and HTTP method used. The user specified requests or responses will appear in the Burp Suite modules without the need to pass through Burp Suite first, or a need to be copied in manually. 

| Action |  HTTP method | URL | 
| ------ | ------ | ------ |
| Send to Repeater | POST | localhost:4567/repeater |
| Send to Inruder | POST | localhost:4567/intruder |
| Send to Comparer |POST | localhost:4567/comparer |
| Add domain to Scope | POST | localhost:4567/scope/*domain* |
| Exclude domain from Scope | DELETE | localhost:4567/scope/*domain* |
| Get whole sitemap | GET | localhost:4567/target |
| Spider domain | POST | localhost:4567/spider/*domain* |
| Perform passive scan | POST | localhost:4567/scanner/passive |
| Perform active scan | POST | localhost:4567/scanner/active |
| Get all issues of a domain | GET | localhost:4567/scanner/issues/*domain* |
| Get all found issues | GET | localhost:4567/scanner/issues |
| Get a list of domains in scope | GET | localhost:4567/targetlist |
| Get a number of issues by severity | GET | localhost:4567/graph/*domain* |
| Get decription of issues found for a domain | GET | localhost:4567/target/issuesinfo/*domain* |
| Total number of found issues | GET | localhost:4567/scanner/total |

Note: the scanner functionality depends on the version of Burp Suite used (the scanner is not accessible to community users).

## How to run the module

- Download or clone the repo
- Navigate into the folder
- Run the following command to generate .jar file    
```sh
    mvn clean package
```
- Open Burp Suite and navigate to the Extender tab
- Click on Extensions Tab, then click Add
- In the newly opened Extension Details window, choose Extension type as Java and navigate to previously generated .jar file
- Click next and you should see the module in the extensions list
- Make sure the "Loaded" checkbox is checked

## Dependencies

The module was developed with Java openjdk 11.0.8 2020-07-14. To compile the source into a .jar file, Maven is required. There are no other explicit dependencies. 

## Examples

The requests or responses user wishes to input to Burp Suite has to be Base64 encoded in the JSON object. Let's consider user wants to send following request:
```sh
   GET / HTTP/1.1
Host: hackthissite.org
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
DNT: 1
Upgrade-Insecure-Requests: 1
Connection: close


```
into the Repeater module. First it needs to be Base64 encoded, which yields this payload:
```sh
R0VUIC8gSFRUUC8xLjEKSG9zdDogaGFja3RoaXNzaXRlLm9yZwpVc2VyLUFnZW50OiBNb3ppbGxhLzUuMCAoV2luZG93cyBOVCAxMC4wOyBXaW42NDsgeDY0OyBydjo4OC4wKSBHZWNrby8yMDEwMDEwMSBGaXJlZm94Lzg4LjAKQWNjZXB0OiB0ZXh0L2h0bWwsYXBwbGljYXRpb24veGh0bWwreG1sLGFwcGxpY2F0aW9uL3htbDtxPTAuOSxpbWFnZS93ZWJwLCovKjtxPTAuOApBY2NlcHQtTGFuZ3VhZ2U6IGVuLVVTLGVuO3E9MC41CkFjY2VwdC1FbmNvZGluZzogZ3ppcCwgZGVmbGF0ZQpETlQ6IDEKVXBncmFkZS1JbnNlY3VyZS1SZXF1ZXN0czogMQpDb25uZWN0aW9uOiBjbG9zZQoK
```
Finally, user uses cURL client to send the request with command:

```sh
curl -X POST 127.0.0.1:4567/repeater -H "Content-Type: application/json" --data '{"comment":null,"request":"R0VUIC8gSFRUUC8xLjEKSG9zdDogaGFja3RoaXNzaXRlLm9yZwpVc2VyLUFnZW50OiBNb3ppbGxhLzUuMCAoV2luZG93cyBOVCAxMC4wOyBXaW42NDsgeDY0OyBydjo4OC4wKSBHZWNrby8yMDEwMDEwMSBGaXJlZm94Lzg4LjAKQWNjZXB0OiB0ZXh0L2h0bWwsYXBwbGljYXRpb24veGh0bWwreG1sLGFwcGxpY2F0aW9uL3htbDtxPTAuOSxpbWFnZS93ZWJwLCovKjtxPTAuOApBY2NlcHQtTGFuZ3VhZ2U6IGVuLVVTLGVuO3E9MC41CkFjY2VwdC1FbmNvZGluZzogZ3ppcCwgZGVmbGF0ZQpETlQ6IDEKVXBncmFkZS1JbnNlY3VyZS1SZXF1ZXN0czogMQpDb25uZWN0aW9uOiBjbG9zZQoK","response":"","highlight":null,"httpService":{"port":443,"protocol":"https","host":"www.hackthissite.org"}}'
```
The request will then be presented to user in Burp Suite's Repeater module.

The JSON object used to describe the request or response has following structure:

```json
{
  "comment": "",
  "request": "",
  "response": "",
  "highlight": "",
  "httpService": {
    "port": "",
    "protocol": "",
    "host": ""
  }
}
```
Empty values can either be null or empty string. In passive scan example if there is no response to be analysed, the response value has to be empty string. In active scan, this value can be null. 

Example of active scan:
```sh
curl -X POST 127.0.0.1:4567/scanner/active -H "Content-Type: application/json" --data '{"comment":null,"request":"R0VUIC8gSFRUUC8xLjEKSG9zdDogaGFja3RoaXNzaXRlLm9yZwpVc2VyLUFnZW50OiBNb3ppbGxhLzUuMCAoV2luZG93cyBOVCAxMC4wOyBXaW42NDsgeDY0OyBydjo4OC4wKSBHZWNrby8yMDEwMDEwMSBGaXJlZm94Lzg4LjAKQWNjZXB0OiB0ZXh0L2h0bWwsYXBwbGljYXRpb24veGh0bWwreG1sLGFwcGxpY2F0aW9uL3htbDtxPTAuOSxpbWFnZS93ZWJwLCovKjtxPTAuOApBY2NlcHQtTGFuZ3VhZ2U6IGVuLVVTLGVuO3E9MC41CkFjY2VwdC1FbmNvZGluZzogZ3ppcCwgZGVmbGF0ZQpETlQ6IDEKVXBncmFkZS1JbnNlY3VyZS1SZXF1ZXN0czogMQpDb25uZWN0aW9uOiBjbG9zZQoK","response":"","highlight":null,"httpService":{"port":443,"protocol":"https","host":"www.hackthissite.org"}}'
```
In this example user wants to perform an active scan on the embedded request represented in the request value.


Example of passive scan:

```sh
curl -X POST 127.0.0.1:4567/scanner/passive -H "Content-Type: application/json" --data '{"comment":null,"request":"R0VUIC8gSFRUUC8xLjEKSG9zdDogd3d3LmhhY2t0aGlzc2l0ZS5vcmcKVXNlci1BZ2VudDogTW96aWxsYS81LjAgKFdpbmRvd3MgTlQgMTAuMDsgV2luNjQ7IHg2NDsgcnY6ODIuMCkgR2Vja28vMjAxMDAxMDEgRmlyZWZveC84Mi4wCkFjY2VwdDogdGV4dC9odG1sLGFwcGxpY2F0aW9uL3hodG1sK3htbCxhcHBsaWNhdGlvbi94bWw7cT0wLjksaW1hZ2Uvd2VicCwqLyo7cT0wLjgKQWNjZXB0LUxhbmd1YWdlOiBlbi1VUyxlbjtxPTAuNQpBY2NlcHQtRW5jb2Rpbmc6IGd6aXAsIGRlZmxhdGUKRE5UOiAxCkNvbm5lY3Rpb246IGNsb3NlCkNvb2tpZTogSGFja1RoaXNTaXRlPTc3czlyc2x2czQwZ2h2djZrZmNwaDJlbGQ3ClVwZ3JhZGUtSW5zZWN1cmUtUmVxdWVzdHM6IDE=","response":"","highlight":null,"httpService":{"port":443,"protocol":"https","host":"www.hackthissite.org"}}'
```
Example of adding a domain to the scope:

```sh
curl -X POST localhost:4567/scope/www.hackthissite.org
```
Example of getting the issues found for a domain hackthissite.org:
```sh
curl localhost:4567/scanner/issues/hackthissite.org
```

## License
 This module implements REST API into Burp Suite primarly used to exchange data with Burp Suite
    Copyright (C) 2021 Radoslav Heriban
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.