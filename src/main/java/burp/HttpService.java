package burp;


public class HttpService implements MyHttpService{

	private String host;
	private int port;
	private String protocol;
		
	public HttpService() {
		
	}
	
	@Override
	public String getHost() {
		return this.host;
	}

	public void setHost(String host) {
		this.host = host;
	}
	
	@Override
	public int getPort() {
		return this.port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
	@Override
	public String getProtocol() {
		return this.protocol;
	}
	
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	
	public boolean usesHttps() {
		
		if(this.protocol.equals("https")) {
			return true;
		}
		return false;
	}
	

}
