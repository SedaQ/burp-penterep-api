package burp;

import java.util.Objects;

public class jsonDomainInfoObj {

	
	private String name;
	private String severity;
	
	public jsonDomainInfoObj(){
		
	};
	
	
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	
    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    // if both the object references are
    // referring to the same object.
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
  
        // type casting of the argument.
        jsonDomainInfoObj other = (jsonDomainInfoObj)obj;
  
        // comparing the state of argument with
        // the state of 'this' Object
        if (name != other.name)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        }
        else if (!name.equals(other.name))
            return false;
        return true;
    }
	
}
