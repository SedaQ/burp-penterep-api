package burp;

public class jsonHelperObj {

	private String name;
	private int value;
	private String color;
	
	public jsonHelperObj(){
		
	};
	
	
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
}
