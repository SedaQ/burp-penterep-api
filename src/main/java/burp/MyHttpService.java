package burp;

public interface MyHttpService extends IHttpService {

	boolean usesHttps();

	public String getHost();

	public int getPort();

	public String getProtocol();


}
