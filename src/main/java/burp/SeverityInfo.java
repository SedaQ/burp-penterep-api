package burp;

public class SeverityInfo {

	private int Low = 0;
	private int Medium = 0;
	private int High = 0;
	private int Information = 0;
	private int FalsePositive = 0;

	public SeverityInfo() {

	}

	public int getLow() {
		return Low;
	}

	public void setLow(int low) {
		Low = low;
	}

	public int getMedium() {
		return Medium;
	}

	public void setMedium(int medium) {
		Medium = medium;
	}

	public int getHigh() {
		return High;
	}

	public void setHigh(int high) {
		High = high;
	}

	public int getInformation() {
		return Information;
	}

	public void setInformation(int information) {
		Information = information;
	}

	public int getFalsePositive() {
		return FalsePositive;
	}

	public void setFalsePositive(int falsePositive) {
		FalsePositive = falsePositive;
	}

	public void addLow() {
		this.Low++;
	}
	
	public void addMedium() {
		this.Medium++;
	}
	
	public void addHigh() {
		this.High++;
	
	}
	
	public void addInformation() {
		this.Information++;
	}
	
	public void addFalsePositive() {
		this.FalsePositive++;
	}
	
	
	public void resetCounters() {
		setLow(0);
		setMedium(0);
		setHigh(0);
		setInformation(0);
		setFalsePositive(0);
		
	}
	
}
